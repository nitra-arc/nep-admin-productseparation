<?php

namespace Nitra\ProductSeparationBundle\Form\Type\Showcase;

use Admingenerated\NitraProductSeparationBundle\Form\BaseShowcaseType\NewType as BaseNewType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NewType extends BaseNewType
{
    protected $subscriber;
    
    public function __construct(EventSubscriberInterface $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->addEventSubscriber($this->subscriber);
    }
}