<?php

namespace Nitra\ProductSeparationBundle\Form\Type\Archive;

use Admingenerated\NitraProductSeparationBundle\Form\BaseArchiveType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EditType extends BaseEditType
{
    protected $subscriber;
    
    public function __construct(EventSubscriberInterface $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->addEventSubscriber($this->subscriber);
    }
}