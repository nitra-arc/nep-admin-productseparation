# ProductSeparationBundle

## Назанчение
Бандл для разделения товаров на группы.
Группу возможно изменить в низу первой вкладки при редактировании товара
По умолчанию доступно три группы:

* Новые товары (Группа, в которую товар попадает при создании. В базе данных (mongoDB) у товара в поле status ставится new_products)
* Витрина (Группа, товары которой должны выводится на сайте. В базе данных (mongoDB) у товара в поле status ставится showcase)
* Архив (Архив не активных товаров - дополнительная группа. В базе данных (mongoDB) у товара в поле status ставится archive)

Реализовано через создание новых генераторов, и, соответственно, контроллеров, роутов и шаблонов.

```yaml
    generator: admingenerator.generator.doctrine_odm
    params:
        model: Nitra\ProductBundle\Document\Product
        namespace_prefix: Nitra
        bundle_name: ProductSeparationBundle
        i18n_catalog: NitraProductBundle
        #...
```

В ListController-е каждой группы при построении запроса в базу данных добавлено условие на поле status с соответствующим значением.

```php
    <?php

    namespace Nitra\ProductSeparationBundle\Controller\Archive;

    use Admingenerated\NitraProductSeparationBundle\BaseArchiveController\ListController as BaseListController;

    class ListController extends BaseListController
    {
        protected function buildQuery()
        {
            $session = $this->getRequest()->getSession();
            
            $dm = parent::buildQuery();
            $dm->addAnd($dm->expr()->field('status')->equals('archive'));
            //выводим товары текущего магазина и товары не относящиеся ни к одному из магазинов
            $dm->addOr($dm->expr()->field('stores.$id')->equals(new \MongoId($session->get('store_id'))));
            $dm->addOr($dm->expr()->field('stores')->exists(false));
                
            return $dm;
        }
    }
```

Edit и New Контроллеры наследуют контроллеры ProductBundle соответственно, но в них переопределяются основные роуты и шаблоны

```php
    <?php

    namespace Nitra\ProductSeparationBundle\Controller\Archive;

    use Nitra\ProductBundle\Controller\Product\EditController as BaseEditController;

    class EditController extends BaseEditController
    {
        public function getNewRoute()
        {
            return 'Nitra_ProductSeparationBundle_Archive_new';
        }
        public function getListRoute()
        {
            return 'Nitra_ProductSeparationBundle_Archive_list';
        }
        public function getEditRoute()
        {
            return 'Nitra_ProductSeparationBundle_Archive_edit';
        }
        public function getEditTwig()
        {
            return 'NitraProductSeparationBundle:ArchiveEdit:index.html.twig';
        }
        public function getUpdateRoute()
        {
            return 'Nitra_ProductSeparationBundle_Archive_update';
        }
        public function getCreateRoute()
        {
            return 'Nitra_ProductSeparationBundle_Archive_create';
        }
    }
```