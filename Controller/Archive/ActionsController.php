<?php

namespace Nitra\ProductSeparationBundle\Controller\Archive;

use Admingenerated\NitraProductSeparationBundle\BaseArchiveController\ActionsController as BaseActionsController;

class ActionsController extends BaseActionsController
{
    protected $listRoute = 'Nitra_ProductSeparationBundle_Archive_list';
    
    use \Nitra\ProductBundle\Traits\ProductActions;
}