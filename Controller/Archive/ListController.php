<?php

namespace Nitra\ProductSeparationBundle\Controller\Archive;

use Admingenerated\NitraProductSeparationBundle\BaseArchiveController\ListController as BaseListController;

/**
 * ListController
 */
class ListController extends BaseListController
{
    protected function buildQuery()
    {
        $session = $this->getRequest()->getSession();
        
        $dm = parent::buildQuery();
        $dm->addAnd($dm->expr()->field('status')->equals('archive'));
        //выводим товары текущего магазина и товары не относящиеся ни к одному из магазинов
        $dm->addOr($dm->expr()->field('stores.$id')->equals(new \MongoId($session->get('store_id'))));
        $dm->addOr($dm->expr()->field('stores')->exists(false));
        
        return $dm;
    }
}
