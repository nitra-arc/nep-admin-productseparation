<?php

namespace Nitra\ProductSeparationBundle\Controller\Archive;

use Admingenerated\NitraProductSeparationBundle\BaseArchiveController\NewController as BaseNewController;
use Nitra\ProductSeparationBundle\Form\Type\Archive\NewType;

class NewController extends BaseNewController
{
    /**
     * @return \Nitra\ProductBundle\Form\Type\Product\NewType
     */
    protected function getNewType()
    {
        $type = new NewType($this->get('product.subscriber'));
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }

    /**
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\ProductBundle\Document\Product $Product
     */
    public function preSave($form, $Product)
    {
        $this->get('product.saver')->preSave($form, $Product, true);
    }

    /**
     * @param \Nitra\ProductBundle\Document\Product $Product
     * @return array
     */
    public function getAdditionalRenderParameters($Product)
    {
        return array(
            'model_editor'      => $this->container->hasParameter('model_editor') ? $this->container->getParameter('model_editor') : false,
        );
    }
}