<?php

namespace Nitra\ProductSeparationBundle\Controller\Newest;

use Admingenerated\NitraProductSeparationBundle\BaseNewestController\ListController as BaseListController;

/**
 * ListController
 */
class ListController extends BaseListController
{
    protected function buildQuery()
    {
        $session = $this->getRequest()->getSession();
        
        $dm = parent::buildQuery();
        $dm->addAnd($dm->expr()->field('status')->equals('new_products'));
        //выводим товары текущего магазина и товары не относящиеся ни к одному из магазинов
        $dm->addOr($dm->expr()->field('stores.$id')->equals(new \MongoId($session->get('store_id'))));
        $dm->addOr($dm->expr()->field('stores')->exists(false));
        
        return $dm;
    }
}
