<?php

namespace Nitra\ProductSeparationBundle\Controller\Newest;

use Admingenerated\NitraProductSeparationBundle\BaseNewestController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Nitra\ProductSeparationBundle\Form\Type\Newest\EditType;

class EditController extends BaseEditController
{
    /**
     * @param string $pk
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction($pk)
    {
        $response = parent::updateAction($pk);
        if (($response instanceof RedirectResponse) && $this->getRequest()->request->has('save-and-update')) {
            $Product = $this->getObject($pk);
            $this->get('product.saver')->updateModelProducts($Product);
            return new RedirectResponse($this->generateUrl('Nitra_ProductSeparationBundle_Newest_edit', array('pk' => $pk)));
        } else {
            return $response;
        }
    }

    /**
     * @return \Nitra\ProductBundle\Form\Type\Product\EditType
     */
    protected function getEditType()
    {
        $type = new EditType($this->get('product.subscriber'));
        $type->setSecurityContext($this->get('security.context'));

        return $type;
    }
    
    /**
     * @param \Nitra\ProductBundle\Document\Product $Product
     * @return array
     */
    protected function getAdditionalRenderParameters($Product)
    {
        return array(
            'model_editor'      => $this->container->hasParameter('model_editor') ? $this->container->getParameter('model_editor') : false,
            'save_and_update'   => $this->container->hasParameter('allow_update_model_products') ? $this->container->getParameter('allow_update_model_products') : false,
        );
    }
    
    /**
     * @param string $pk
     * @return \Nitra\ProductBundle\Document\Product
     */
    protected function getObject($pk)
    {
        $Product = parent::getObject($pk);
        
        $this->get('product.saver')->addAllowedParameters($Product);
            
        return $Product;
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     * @param \Nitra\ProductBundle\Document\Product $Product
     */
    public function preSave($form, $Product)
    {
        $this->get('product.saver')->preSave($form, $Product);
    }
    
    /**
     * @param \Symfony\Component\Form\Form $form
     * @param \Nitra\ProductBundle\Document\Product $Product
     */
    public function postSave($form, $Product)
    {
        $this->get('product.saver')->postSave($Product);
    }
}