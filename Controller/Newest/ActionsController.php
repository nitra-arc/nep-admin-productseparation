<?php

namespace Nitra\ProductSeparationBundle\Controller\Newest;

use Admingenerated\NitraProductSeparationBundle\BaseNewestController\ActionsController as BaseActionsController;

class ActionsController extends BaseActionsController
{
    protected $listRoute = 'Nitra_ProductSeparationBundle_Newest_list';
    
    use \Nitra\ProductBundle\Traits\ProductActions;
}