<?php

namespace Nitra\ProductSeparationBundle\Controller\Showcase;

use Admingenerated\NitraProductSeparationBundle\BaseShowcaseController\ListController as BaseListController;

/**
 * ListController
 */
class ListController extends BaseListController
{
    protected function buildQuery()
    {
        $session = $this->getRequest()->getSession();
        
        $qb = parent::buildQuery();
        $qb->addAnd(
            $qb->expr()->addOr(
                $qb->expr()->field('status')->equals('showcase')
            )->addOr(
                $qb->expr()->field('status')->exists(false)
            )
        );
        $qb->addAnd(
            $qb->expr()->addOr(
                $qb->expr()->field('stores.id')->equals($session->get('store_id'))
            )->addOr(
                $qb->expr()->field('stores')->exists(false)
            )
        );
        
        return $qb;
    }
}