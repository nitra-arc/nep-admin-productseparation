<?php

namespace Nitra\ProductSeparationBundle\Controller\Showcase;

use Admingenerated\NitraProductSeparationBundle\BaseShowcaseController\ActionsController as BaseActionsController;

class ActionsController extends BaseActionsController
{
    protected $listRoute = 'Nitra_ProductSeparationBundle_Showcase_list';
    
    use \Nitra\ProductBundle\Traits\ProductActions;
}