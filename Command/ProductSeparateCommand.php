<?php

namespace Nitra\ProductSeparationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class ProductSeparateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('product:separate')
            ->setDescription('Separation products')
            ->addArgument('product-repository', InputArgument::OPTIONAL, 'Repository of product', 'NitraProductBundle:Product');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        // перемещение товаров, у которых "Снять с витрины до.." прошло, на витрину
        $toShowcase = $dm->getRepository($input->getArgument('product-repository'))->findBy(array(
            'status'       => 'clarification',
            'removeToDate' => array(
                '$lte' => new \MongoDate(microtime(true)),
            ),
        ));
        $i = 0;
        foreach ($toShowcase as $product) {
            $product->setStatus('showcase');
            $dm->persist($product);
            if ($i % 200 == 0) {
                $dm->flush();
            }
            $i++;
        }
        $output->writeln('Moved to showcase: ' . ($i) . ' products');
        // перемещение товаров, у которых «нет в наличии» более 1 месяца, в архив
        $toArchive = $dm->getRepository('NitraProductBundle:Product')->findBy(array(
            'status'        => 'showcase',
            'stock'         => 'notInStock',
            'updatedAt'     => array(
                '$lte'          => new \MongoDate(microtime(true) - 30 * 24 * 60 * 60),     // откат на месяц назад (30 дней)
            ),
        ));
        $j = 0;
        foreach ($toArchive as $product) {
            $product->setStatus('archive');
            $dm->persist($product);
            if ($j % 200 == 0) {
                $dm->flush();
            }
            $j++;
        }
        $output->writeln('Moved to archive: ' . ($j) . ' products');
        $dm->flush();
        $output->writeln('Separation completed.');
    }
}